package com.expeditors.training.course3demo.model;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

public class Card {
	String name;
	
	@NotBlank
	@Pattern(regexp="(\\d{4}\\-?){3}\\d{4}")
	String number;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = formatAsCreditCard(number);
	}
	public String getMaskedNumber() {
		return maskCreditCardNumber(number);
	}
	
	String maskCreditCardNumber(String str) {
		return
				str
				.substring(
						0,
						str.length() - 4)
				.replaceAll("\\d", "*") + 
				str.substring(
						str.length() - 4,
						str.length());
	}
	
	String formatAsCreditCard(String str) {
		String digits = str.replaceAll("[^\\d]", "");
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0; i < digits.length();) {
			sb.append(digits.charAt(i++))
			.append((i % 4 == 0 && i < digits.length()) ? '-' : "");
		}
		
		return sb.toString();
	}
}
