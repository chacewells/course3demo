package com.expeditors.training.course3demo.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.expeditors.training.course3demo.model.Product;
import com.expeditors.training.course3demo.service.ProductService;

@Controller
@RequestMapping("/product/")
public class ProductController {
	private static final Logger logger = LoggerFactory.getLogger(ProductController.class);
	
	@Autowired
	private ProductService service;
	
	@RequestMapping(method=RequestMethod.GET, value="show.html")
	public ModelAndView showProduct(@RequestParam("product_id") long id) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("showProduct");
		logger.debug("Setting name to Tea");
		mav.addObject("product", service.getProduct(id));
		return mav;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="add.html")
	public ModelAndView addProduct() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("addProduct");
		Product product = new Product();
		product.setPrice(2.99);
		mav.addObject("product", product);
		return mav;
	}
	
	@RequestMapping(method=RequestMethod.POST, value="add.html")
	public ModelAndView addProduct(@ModelAttribute("product") @Valid Product product,
			BindingResult result,
			Model m) {
		ModelAndView mav = new ModelAndView();
		mav.addObject("product", product);
		
		mav.setViewName(result.hasErrors() ? "addProduct" : "showProduct");
		
		return mav;
	}
	
}
