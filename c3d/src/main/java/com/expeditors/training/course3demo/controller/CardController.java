package com.expeditors.training.course3demo.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.expeditors.training.course3demo.model.Card;

@Controller
@RequestMapping("/card/")
public class CardController {
	Logger logger = LoggerFactory.getLogger(CardController.class);
	
	@RequestMapping("add.html")
	public ModelAndView showAddCard(
			@AuthenticationPrincipal User activeUser) {
		ModelAndView mav = new ModelAndView();
		mav.addObject("card", new Card());
		mav.setViewName("addCard");
		return mav;
	}
	
	@RequestMapping(method=RequestMethod.POST, value="add.html")
	public ModelAndView doAddCard(@ModelAttribute @Valid Card card,
			BindingResult result,
			Model m) {
		ModelAndView mav = new ModelAndView();
		mav.addObject("card", card);
		List<ObjectError> coll = result.getAllErrors();
		logger.info("card name: " + card.getName());
		logger.info("card number: " + card.getNumber());
		for (ObjectError oe : coll) {
			logger.info(oe.toString());
		}
		mav.setViewName( result.hasErrors() ? "addCard" : "showCard" );
		return mav;
	}
	
}
