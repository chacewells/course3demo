package com.expeditors.training.course3demo.service;

import org.springframework.stereotype.Service;

import com.expeditors.training.course3demo.model.Product;

@Service
public class ProductService {
	public Product getProduct(long id) {
		return new Product("Latte", "coffee", 2.99);
	}
}
