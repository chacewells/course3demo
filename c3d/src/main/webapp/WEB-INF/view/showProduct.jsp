<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page language="java" %>
<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    
<html>
	<head>
		<title>Product Info: ${product.name}</title>
	</head> 
	<body>
		<h2>${product.name} - <fmt:formatNumber type="currency" value="${product.price}" /></h2>
		<p>${product.description}</p>
	</body>
</html>
