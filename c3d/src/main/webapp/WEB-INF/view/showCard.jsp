<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page language="java" %>
<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    
<html>
	<head>
		<title>Card Info: ${card.name}</title>
	</head> 
	<body>
		<h2>${card.name}</h2>
		<p>${card.maskedNumber}</p>
	</body>
</html>
