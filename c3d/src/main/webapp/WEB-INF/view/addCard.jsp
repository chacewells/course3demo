<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<title>Add a Card for <sec:authentication property='principal.username' /></title>
</head>
<body>
<form:form method="POST" commandName="card">
	<table>
		<tr>
			<td>Name:</td>
			<td><form:input path="name"/></td>
			<td><form:errors path="name" cssClass="error"/></td>
		</tr>
		<tr>
			<td>Number:</td>
			<td><form:input path="number"/></td>
			<td><form:errors path="number" cssClass="error"/></td>
		</tr>
		<tr>
			<td colspan="2"><input type="submit" value="Add Card"/></td>
		</tr>
	</table>
</form:form>
</body>
</html>