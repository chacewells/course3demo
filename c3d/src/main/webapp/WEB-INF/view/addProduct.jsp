<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add a Product</title>
<link rel="stylesheet" type="text/css" href='<c:url value="/static/css/my.css" />' />
</head>
<body>
<form:form method="POST" commandName="product">
	<div class="container">
		<div class="row">
			<p class="medium-6 small-text-center medium-text-right columns">Name:</p>
			<p class="small-6 small-offset-3 medium-offset-0 medium-3 columns">
				<form:input path="name" />
			</p>
			<p class="medium-3 columns" style="color:red"><form:errors path="name" cssClass="error" /></p>
		</div>
		<div class="row">
			<p class="medium-3 small-text-center medium-offset-3 medium-text-right columns">Description:</p>
			<p class="medium-3 medium-pull-3 columns">
				<form:input path="description" />
			</p>
		</div>
		<div class="row">
			<p class="medium-3 medium-offset-3 medium-text-right columns">Price:</p>
			<p class="small-6 small-pull-3 medium-3 medium-pull-3 columns">
				<form:input style="text-align:center" path="price" />
			</p>
		</div>
		<div class="row">
			<p class="small-3 small-centered columns">
				<input type="submit" class="button" value="Add Product" />
			</p>
		</div>
	</div>
</form:form>
</body>
</html>