<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><decorator:title /></title>
</head>
<body>
    <h1><decorator:title /></h1>
    <p><b>Navigation</b></p>
    <hr />
    <decorator:body />
    <hr />
    <h2><decorator:title /></h2>
    <p>&copy; Expeditors International Training</p>
</body>
</html>