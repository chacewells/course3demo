<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:url value="/static/css/foundation.min.css" var="foundation_css" />
<c:url value="/static/js/vendor/modernizr.js" var="modernizr_js" />

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<decorator:head />
	<title><decorator:title /></title>
	
	<!-- stylesheets -->
	<link rel="stylesheet" href="${foundation_css}" />
	
	<!-- preloaded scripts -->
	<script type="text/javascript" src="${modernizr_js}"></script>
</head>
<body>
	<header class="row">
	    <h1 class="small-text-center"><decorator:title /></h1>
	</header>
    <hr />
    <decorator:body />
    <hr />
    <footer class="row">
	    <p class="small-text-center">&copy; 2015 Expeditors Training</p>
    </footer>
</body>
</html>